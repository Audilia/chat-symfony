<?php

namespace App\Controller;

use App\Entity\Channel;
use App\Entity\Message;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * @Route("/messages", name="chat")
 */
class ChatController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/all", name="chat")
     */
    public function index()
    {
        $message = $this->getDoctrine()
            ->getRepository(Message::class)
            ->findAll();

        $data = $this->serializer->serialize($message, 'json');

        return new Response($data);

    }

    /**
     * @Route("/add", name="addMessage", methods="POST")
     */
    public function addMessage(Request $request)
    {
        $data = $request->getContent();
        $data = json_decode($data, true);
        $userID = $data["user_id"];
        $content = $data["content"];

        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userID);

        $message = new Message();
        $message->setUser($user);
        $message->setContent($content);

        $entityManager->persist($message);
        $entityManager->flush();

        return new JsonResponse($data);
    }

    /**
     * @Route("/delete/{id}", name="deleteMessage", methods="DELETE")
     */

     public function deleteMessage(Request $request, $id)
     {
        $data = $request->getContent();
        $data = json_decode($data, true);

        $entityManager = $this->getDoctrine()->getManager();
        $message = $entityManager
            ->getRepository(Message::class)
            ->find($id);

        if (!$message) {
            throw $this->createNotFoundException(
                'Aucune correspondance trouvée pour ' . $id
            );
        }

        $entityManager->remove($message);
        $entityManager->flush();

        return new JsonResponse(['message supprimé']);
     }

    /**
     * @Route("/show/{id}", name="showId", methods="GET")
     */

    public function show() {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        $data = $this->serializer->serialize($user, 'json');

        return new Response($data);

    }
}
