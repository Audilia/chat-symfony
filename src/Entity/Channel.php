<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChannelRepository")
 */
class Channel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChannelUser", mappedBy="channel")
     */
    private $channelUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="channel")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server", inversedBy="channel")
     */
    private $server;

    public function __construct()
    {
        $this->channelUsers = new ArrayCollection();
        $this->message = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ChannelUser[]
     */
    public function getChannelUsers(): Collection
    {
        return $this->channelUsers;
    }

    public function addChannelUser(ChannelUser $channelUser): self
    {
        if (!$this->channelUsers->contains($channelUser)) {
            $this->channelUsers[] = $channelUser;
            $channelUser->setChannel($this);
        }

        return $this;
    }

    public function removeChannelUser(ChannelUser $channelUser): self
    {
        if ($this->channelUsers->contains($channelUser)) {
            $this->channelUsers->removeElement($channelUser);
            // set the owning side to null (unless already changed)
            if ($channelUser->getChannel() === $this) {
                $channelUser->setChannel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessage(): Collection
    {
        return $this->message;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->message->contains($message)) {
            $this->message[] = $message;
            $message->setChannel($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->message->contains($message)) {
            $this->message->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getChannel() === $this) {
                $message->setChannel(null);
            }
        }

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(?Server $server): self
    {
        $this->server = $server;

        return $this;
    }
}
