<?php

namespace App\Repository;

use App\Entity\ChannelUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ChannelUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChannelUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChannelUser[]    findAll()
 * @method ChannelUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChannelUser::class);
    }

//    /**
//     * @return ChannelUser[] Returns an array of ChannelUser objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChannelUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
